﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectoEntorno.Common;



namespace ProjectoEntorno.Biz
{
    /// <summary>
    /// Clase operacion, para los metodos suma resta y multiplicacion
    /// </summary>
    public class Operacion
    {
        #region atributos

        DateTime fecha = DateTime.Today;
        
        private int Num1;
        private int Num2;
        public readonly int CualEsMayor;
        #endregion

        #region constructor
        public Operacion(int n1, int n2)
        {
            
            if (Num1 > Num2)
            {
                this.CualEsMayor = n1;
                Num2 = n2;
                Num1 = n1;
            }
            else
            {
                this.CualEsMayor = n2;
                Num2 = n1;
                Num1 = n2;
            }
        }
        #endregion

        #region Metodos
        /// <summary>
        /// Metodo Suma, suma dos numeros
        /// </summary>

        public string Suma()
        {
            DateTime fechaActual = DateTime.Today;
            string mensaje = string.Format("El dia de la semana es: {0}\n",CosasDeFechas.DiaSemana(fechaActual));
            int resultado = Num1 + Num2;
            mensaje += string.Format("Resultado de la operacion: {0}",resultado.ToString());
            return mensaje;
        }
        /// <summary>
        /// Metedo Resta, resta el numero mayor al menor
        /// </summary>
        
        public string Resta()
        {
            DateTime fechaActual = DateTime.Today;
            string mensaje = string.Format("El dia de la semana es: {0}\n", CosasDeFechas.DiaSemana(fechaActual));
            int resultado = CualEsMayor - Num2;
            mensaje += string.Format("Resultado de la operacion: {0}", resultado.ToString());
            return mensaje;
        }

        /// <summary>
        /// Multiplica dos valores
        /// </summary>
        
        public string Multiplicacion()
        {
            DateTime fechaActual = DateTime.Today;
            string mensaje = string.Format("El dia de la semana es: {0}\n", CosasDeFechas.DiaSemana(fechaActual));
            int resultado = Num1 * Num2;
            mensaje += string.Format("Resultado de la operacion: {0}", resultado.ToString());
            return mensaje;
        }
        #endregion
    }
}
