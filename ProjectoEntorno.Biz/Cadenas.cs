﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectoEntorno.Biz
{
    /// <summary>
    /// Clase para manipular strings
    /// </summary>
    public class Cadenas
    {

        #region Constructor
        /// <summary>
        /// Constructor de la clase, se le pasa por parametro un string
        /// </summary>
        /// <param name="frase"></param>
        public Cadenas(string Frase)
        {
        }
        #endregion

        #region Metodos de interfaz
        /// <summary>
        /// Metodo para extraer el primer caracter de un string y devuelve el caracter
        /// </summary>
        /// <param name="fras"></param>
        /// <returns></returns>
        public string ExtraerPrimerCaracter(string fras)
        {
            return fras[0].ToString();
        }
        /// <summary>
        /// Metodo para extraer el ultimo caracter de un string, y devuelve el caracter
        /// </summary>
        /// <param name="fras"></param>
        /// <returns></returns>
        public string ExtraerUltimoCaracter(string fras)
        {
            return fras[fras.Length - 1].ToString();
        }
        /// <summary>
        /// Metodo para extraer el caracter de una posicion pasada como parametro, y devuelve el caracter
        /// </summary>
        /// <param name="fras"></param>
        /// <param name="posicion"></param>
        /// <returns></returns>
        public string ExtraerPosicionDeterminada(string fras, int posicion)
        {
            return fras[posicion-1].ToString();
        }

        #endregion

    }
}
