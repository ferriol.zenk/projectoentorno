﻿using System;

namespace ProjectoEntorno.Biz
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Comprobaciones
            Cadenas cadena = new Cadenas("hola");
            Console.WriteLine(cadena.ExtraerPrimerCaracter("hola"));
            Console.WriteLine(cadena.ExtraerUltimoCaracter("hola"));
            Console.WriteLine(cadena.ExtraerPosicionDeterminada("hola",3));

            Operacion op = new Operacion(1, 2);
            Console.WriteLine(op.Suma());
            Console.WriteLine(op.Resta());
            Console.WriteLine(op.Multiplicacion());
            #endregion
        }
    }
}
