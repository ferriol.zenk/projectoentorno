﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectoEntorno.Biz;

namespace BizTest
{
    /// <summary>
    /// Test unitarios de la clase Cadenas
    /// </summary>
    [TestClass]
    public class CadenasTest
    {
        #region Metodos Tests Unitarios
        /// <summary>
        /// Test unitarios para el constructor de la clase
        /// </summary>
        [TestMethod]
        public void ConstructorTest()
        {
            //Arrange
            Cadenas expected = new Cadenas("hola"); //Valor esperado
            //Act
            //Assert
            Assert.IsNotNull(expected);
        }
        /// <summary>
        /// Test unitario para el metodo que obtiene el primer caracter de un string
        /// </summary>
        [TestMethod]
        public void PrimerCaracterTest()
        {
            //Arrange
            string expected = "h";//Valor esperado
            //Act
            Cadenas cadena = new Cadenas("hola");
            string test = cadena.ExtraerPrimerCaracter("hola");
            //Assert
            Assert.AreEqual(expected, test);
        }
        /// <summary>
        /// Test unitario para el metodo que obtiene el ultimo caracter de un string
        /// </summary>
        [TestMethod]
        public void UltimoCaracterTest()
        {
            //Arrange
            string expected = "a";//Valor esperado
            //Act
            Cadenas cadena = new Cadenas("hola");
            string test=cadena.ExtraerUltimoCaracter("hola");
            //Assert
            Assert.AreEqual(expected, test);
        }
        /// <summary>
        /// Test unitario para el metodo que obtiene el caracter de una posicion determinada de un string
        /// </summary>
        [TestMethod]
        public void PosicionDeterminadaCaracterTest()
        {
            //Arrange
            string expected = "l";//Valor esperado
            //Act
            Cadenas cadena = new Cadenas("hola");

            string test=cadena.ExtraerPosicionDeterminada("hola",3);
            //Assert
            Assert.AreEqual(expected, test);
        }
        #endregion
    }
}
