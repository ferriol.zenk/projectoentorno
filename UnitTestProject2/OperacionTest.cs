using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectoEntorno.Biz;
using ProjectoEntorno.Common;
using System;

namespace BizTest
{
    /// <summary>
    /// prueba test unitario Operaciones Test
    /// </summary>
    [TestClass]
    public class OperacionTest
    {
        #region Metodos Tests Unitarios
        /// <summary>
        /// prueba test unitario del constructor
        /// </summary>
        [TestMethod]
        public void ConstructorTest()
        {
            //Arrange
            Operacion expected = new Operacion(2,3); //Valor esperado
            //Act
            //Assert
            Assert.IsNotNull(expected);
        }
        /// <summary>
        /// Test Unitario para el metodo de suma, que devuelve el d�a actual y el resultado de la operacion en un string
        /// </summary>
        [TestMethod]
        public void SumaTest()
        {
            //Arrange
            Operacion op = new Operacion(1, 2);
            DateTime fechaActual = DateTime.Today;
            string expected = string.Format("El dia de la semana es: {0}\n", CosasDeFechas.DiaSemana(fechaActual));//Valor esperado
            expected += string.Format("Resultado de la operacion: {0}", 3.ToString());//Valor esperado
            //Act
            string test = op.Suma();
            //Assert
            Assert.AreEqual(expected, test);     
        }
        /// <summary>
        /// Test Unitario para el metodo de resta, que devuelve el d�a actual y el resultado de la operacion en un string
        /// </summary>
        [TestMethod]
        public void RestaTest()
        {
            //Arrange
            Operacion op = new Operacion(1, 2);
            DateTime fechaActual = DateTime.Today;
            string expected = string.Format("El dia de la semana es: {0}\n", CosasDeFechas.DiaSemana(fechaActual));//Valor esperado
            expected += string.Format("Resultado de la operacion: {0}", 1.ToString());//Valor esperado
            //Act
            string test = op.Resta();
            //Assert
            Assert.AreEqual(expected, test);
        }
        /// <summary>
        /// Test Unitario para el metodo de multiplicacion, que devuelve el d�a actual y el resultado de la operacion en un string
        /// </summary>
        [TestMethod]
        public void MultiplicacionTest()
        {
            //Arrange
            Operacion op = new Operacion(1, 2);
            DateTime fechaActual = DateTime.Today;
            string expected = string.Format("El dia de la semana es: {0}\n", CosasDeFechas.DiaSemana(fechaActual));//Valor esperado
            expected += string.Format("Resultado de la operacion: {0}", 2.ToString());//Valor esperado
            //Act
            string test = op.Multiplicacion();
            //Assert
            Assert.AreEqual(expected, test);
        }
        #endregion
    }
}
