using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectoEntorno.Common;
using System;

namespace CommonTest
{
    
    /// <summary>
    /// Pruebas test unitarios cosas de fechas
    /// </summary>
    [TestClass]
    public class CosasDeFechasTests
    {
        #region Metodos test unitarios
        /// <summary>
        /// prueba test unitario comprobar dia de la semaman 
        /// </summary>
        [TestMethod]
        public void DiaSemanaTest()
        {
            //Arrange
            DateTime fechaActual = DateTime.Today;
            string expected =  fechaActual.ToString("dddd");//Valor esperado
            //Act
            string test = CosasDeFechas.DiaSemana(fechaActual);
            //Assert
            Assert.AreEqual(expected, test);
        }
        /// <summary>
        /// prueba test unitario cuantos dias de la semana faltan para tu cumpleaņos
        /// </summary>
        [TestMethod]
        public void CuantoFaltaTest()
        {
            //Arrange
            string fecha = "12/11/1988";
            DateTime parsedFecha = DateTime.Parse(fecha);
            DateTime nextBday = new DateTime(DateTime.Now.Year, parsedFecha.Month, parsedFecha.Day);
            var expected = (nextBday - DateTime.Today).Days; //Valor esperado
            //Act
            int test = CosasDeFechas.CuantoFalta(fecha);
            //Assert
            Assert.AreEqual(expected, test);
        }
    }
    #endregion
    }
