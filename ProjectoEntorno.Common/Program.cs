﻿using System;

namespace ProjectoEntorno.Common
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Comprobaciones
            DateTime fechaActual = DateTime.Today;
            Console.WriteLine(CosasDeFechas.DiaSemana(fechaActual));
            Console.WriteLine(CosasDeFechas.CuantoFalta("12/11/1988"));
            #endregion
        }
    }
}
