﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectoEntorno.Common
{
    /// <summary>
    /// Clase estatica para manipular fechas
    /// </summary>
    public static class CosasDeFechas
    {
        #region Metodos estaticos
        /// <summary>
        /// Metodo que devuelve el dia actual de una fecha pasada por parametro
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public static string DiaSemana(DateTime fecha)
        {
            return fecha.ToString("dddd");
        }

        /// <summary>
        /// Fecha que calcula los días que quedan hasta el cumpleños, pasando como parametro 
        /// un string con la fecha de nacimiento.
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public static int CuantoFalta(string fecha) {

            DateTime parsedFecha = DateTime.Parse(fecha);
            DateTime nextBday = new DateTime(DateTime.Now.Year, parsedFecha.Month, parsedFecha.Day); 
            if (DateTime.Today > nextBday) nextBday = nextBday.AddYears(1); 
            return (nextBday - DateTime.Today).Days; 

        } 
        #endregion
    }
}
